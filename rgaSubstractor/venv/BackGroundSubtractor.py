import threading

from PyQt5.QtWidgets import QWidget, QDialog, QApplication, QRadioButton
from PyQt5 import QtWidgets
from pyqtgraph import PlotWidget

from widgetSubstractor import PlotScan
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, \
    QHBoxLayout, QVBoxLayout, QMessageBox
import os
import sys
import threading as th


class MainWidget(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWidget, self).__init__(*args, **kwargs)
        widget = QWidget()


        self.assembly = PlotScan()
        self.background = PlotScan()

        self.layout = QVBoxLayout()

        self.plotSubstraction = PlotWidget()
        self.plotSubstraction.showGrid(x=True,y=True)

        self.radio = QRadioButton("SAT Acceptance Limit Line")
        self.radio.toggled.connect(self.radioToggled)

        self.radio.setEnabled(False)

        self.layout.addWidget(self.assembly)
        self.layout.addWidget(self.background)
        self.layout.addWidget(self.plotSubstraction)
        self.layout.addWidget(self.radio)

        self.setWindowTitle("RGA subtract v2")


        self.fileTaker = th.Thread(target=self.set_pressures)
        self.hasFiles = threading.Event()


        self.endUpdateLoop = threading.Event()

        self.fileTaker.start()

        self.pressureToCompare1 = []
        self.pressureToCompare2 = []

        self.massToPlot=[]
        self.subtraction=[]
        self.waterPoss=0
        self.mass50pos=0

        # self.do = True
        widget.setLayout(self.layout)
        self.setCentralWidget(widget)



        self.pressures_1=[]
        self.pressures_2=[]

    def set_pressures(self):

        while not self.hasFiles.is_set():

            print(self.assembly.hasFile, self.background.hasFile)

            if self.assembly.hasFile and self.background.hasFile:
                self.pressureToCompare1 = self.assembly.get_pessures()
                self.pressureToCompare2 = self.background.get_pessures()
                self.hasFiles.set()
                # self.set_hasFile()


                self.pressures_1= self.assembly.get_pessures()
                self.pressures_2 = self.background.get_pessures()

                self.radio.setEnabled(True)
                self.do_subtraction()
                self.files_ready=True

                # self.plotUpdater = th.Thread(target=self.updatePressure())
                # self.plotUpdater.start()


    def updatePressure(self):
        while True:
            while not self.endUpdateLoop.is_set:

                print("WWWWWWWWWWW")
                # while True:
                if self.pressureToCompare1 != self.assembly.get_pessures():
                    print("P=",self.pressureToCompare1)
                    print("P2=",self.assembly.get_pessures())
                    self.subtraction=[]
                    # self.do = True
                    # self.do_subtraction()
                    self.pressureToCompare1 = self.assembly.get_pessures()
                    print("Comparison1 = true")

                else:
                    # self.pressureToCompare1 = self.assembly.get_pessures()
                    print("Comparison1 = false")

                    # self.do= False
                if self.pressureToCompare2 != self.background.get_pessures():
                    self.subtraction = []
                    # self.do_subtraction()
                    # self.do = True
                    self.pressureToCompare2 = self.background.get_pessures()
                    print("Comparison2 = true")
                else:
                    # self.pressureToCompare2 = self.background.get_pessures()
                    print("Comparison2 = false")
                    # self.do= False

    def do_subtraction(self):

        # # if self.do:
        # """from Here """
        # print("WWWWWWWWWWW")
        # # while True:
        # if self.pressureToCompare1 != self.assembly.get_pessures():
        #     print("P=", self.pressureToCompare1)
        #     print("P2=", self.assembly.get_pessures())
        #     self.subtraction = []
        #     # self.do = True
        #     # self.do_subtraction()
        #     self.pressures_1 = self.assembly.get_pessures()
        #
        #     self.pressureToCompare1 = self.assembly.get_pessures()
        #     print("Comparison1 = true")
        #
        # else:
        #     # self.pressureToCompare1 = self.assembly.get_pessures()
        #     print("Comparison1 = false")
        #
        #     # self.do= False
        # if self.pressureToCompare2 != self.background.get_pessures():
        #     self.subtraction = []
        #     # self.do_subtraction()
        #     # self.do = True
        #     self.pressures_2 = self.background.get_pessures()
        #
        #     self.pressureToCompare2 = self.background.get_pessures()
        #     print("Comparison2 = true")
        # else:
        #     # self.pressureToCompare2 = self.background.get_pessures()
        #     print("Comparison2 = false")
        #     # self.do= False
        #     """toHere"""
        #








            # pressures_1 = self.assembly.get_pessures()
            # pressures_2 = self.background.get_pessures()
            print("111=", len(self.pressures_1))
            print("222=", len(self.pressures_2))
            print("11=", self.pressures_1)
            print("22=", self.pressures_2)

            len1 = len(self.assembly.get_masses())
            len2 = len(self.background.get_masses())

            factor = len1 / len2
            print("FACTOR=", factor)
            if factor > 1:
                print("1_1=", len(self.pressures_1))
                pressures_1 = (self.pressures_1[0:-1:int(factor)])
                print("1_1=", len(pressures_1))
                self.massToPlot = self.background.get_masses()

            else:

                factor2 = 1 / factor
                pressures_2 = (self.pressures_2[0:-1:int(factor2)])
                print("1_1=", len(self.pressures_1))
                print("2_2=", len(pressures_2))
                self.massToPlot = self.assembly.get_masses()

            print("1=", len(self.pressures_1))
            print("2=", len(self.pressures_2))
            print("11=", self.pressures_1)
            print("22=", self.pressures_2)

            print("HOLE")
            print("lenSubs ",len(self.subtraction))
            print(self.subtraction)
            # subtraction = []
            for i in range(0, len(self.pressures_1)):
                subs = self.pressures_1[i] - self.pressures_2[i]
                if subs < 1e-15:
                    subs = 1e-15
                self.subtraction.append(subs)

            print("lenSubs ",len(self.subtraction))
            print(self.subtraction)

            self.waterPoss = self.massToPlot.index(18.0)
            self.mass50pos=self.massToPlot.index(50.0)

            self.plotSubstraction.plot(self.massToPlot, self.subtraction)

            # self.pressures_1 = []
            # self.pressures_2 = []
            # self.do_subtraction()

        # self.do = False

    def radioToggled(self):

        if  self.radio.isChecked():
            waterLevel = self.subtraction[self.waterPoss]
            acceptanceLimit = waterLevel * 1e-03
            acceptanceLine=[]
            for i in range (0,self.mass50pos):
                acceptanceLine.append(1e-15)
            # print("AL ",len(acceptanceLine))
            for i in range(self.mass50pos, len(self.subtraction)):
                acceptanceLine.append(acceptanceLimit)
            print("AL ",len(acceptanceLine))
            print("Al ", acceptanceLine)
            print ("MASS", len(self.massToPlot))
            self.plotSubstraction.clear()
            self.plotSubstraction.plot(self.massToPlot, self.subtraction)
            self.plotSubstraction.plot(self.massToPlot, acceptanceLine,pen='r')

        else:
            self.plotSubstraction.clear()
            self.plotSubstraction.plot(self.massToPlot, self.subtraction)







if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = MainWidget()
    mainWin.show()
    sys.exit(app.exec_())



    # app = QApplication(sys.argv)
    # dialog = MainWindow()
    # sys.exit(dialog.exec_())

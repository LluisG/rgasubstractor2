# import pyqtgraph as pg
import os
import sys

import numpy as np
from PyQt5 import QtWidgets
from PyQt5.QtCore import QDir
from PyQt5.QtWidgets import QLabel, QWidget, \
    QHBoxLayout, QVBoxLayout, QMessageBox
from PyQt5.QtWidgets import QPushButton
from pyqtgraph import FileDialog
from pyqtgraph import PlotWidget


class PlotScan(QtWidgets.QMainWindow):
    # def __init__(self, *args, **kwargs):
    #     super(PlotScan, self).__init__(*args, **kwargs)
    #     # self.mainWindow = MainWindow2

    def __init__(self, *args, **kwargs):
        super(PlotScan, self).__init__(*args, **kwargs)
        widget = QWidget()
        # self.setWindowTitle("RgaSubstractor")

        # self.mainWindow = MainWindow2()
        self.hasFile = False

        self.fileButton = QPushButton('Select File', self)
        self.fileButton.clicked.connect(self.clickMethod)
        self.fileButton.resize(100, 32)
        self.fileNameLine = QLabel("File")
        self.fileNameLine.setStyleSheet(" border: 1px solid black")
        self.fileNameLine.setGeometry(400, 400, 500, 300)
        self.fileNameLine.adjustSize()
        self.nScanLine = QLabel("0/0")
        self.nScanLine.setStyleSheet(" border: 1px solid black")

        # self.layout=QGridLayout()
        self.vertical_layout2 = QVBoxLayout()
        self.vertical_layout2.addWidget(self.fileButton)
        self.vertical_layout2.addWidget(self.fileNameLine)
        self.vertical_layout2.addWidget(self.nScanLine)

        # self.main_layout = QHBoxLayout()
        self.plot = PlotWidget()
        self.plot.showGrid(x=True,y=True)

        self.slider = QtWidgets.QSlider(1)
        self.slider.setSingleStep(1)
        self.slider.setTickInterval(1)
        self.slider.setTickPosition(2)

        self.slider.valueChanged.connect(self.sliderChange)

        self.vertical_layout1 = QVBoxLayout()
        self.vertical_layout1.addWidget(self.plot)
        self.vertical_layout1.addWidget(self.slider)

        self.setWindowTitle("RGA File Plotter")

        self.main_layout = QHBoxLayout()

        self.main_layout.addLayout(self.vertical_layout1)
        self.main_layout.addLayout(self.vertical_layout2)
        self.main_layout.minimumHeightForWidth(0.5)

        widget.setLayout(self.main_layout)

        self.setCentralWidget(widget)

        self.massList = []
        self.pressuresList = []
        self.datesList=[]
        self.n_scans = 0
        self.date = ""

    def clickMethod(self):
        self.massList = []
        self.pressuresList = []
        self.n_scans = 0

        fileName, _ = FileDialog.getOpenFileName(self, 'rga txt files',
                                                 QDir.homePath(),
                                                 "RGA txt ( *.txt)")

        if fileName:
            file1 = open(fileName, 'r')
            lines = file1.readlines()
            if not lines[0] == ('"Spectra International Data File"\n'):
                QMessageBox.warning(self, "Not a MKS RGA file",
                                    "Ensure to choose a MKS RGA file")
            else:

                self.fileNameLine.setText(os.path.split(fileName)[-1])
                for line in lines:
                    if line.startswith('"Time"'):
                        self.take_masses(line)

                for line in lines[59:-2]:
                    self.take_pressures(line)
        self.hasFile = True
        # print(self.hasFile)
        self.doPlot(self.slider.value())

    def take_masses(self, line):
        """take masses list in float format"""
        massLine = line.replace('"Mass ', "")
        massLine = massLine.replace('"', '')
        massLine = massLine.split()
        del massLine[0:2]
        del massLine[-3:]
        massLine = [float(x) for x in massLine]
        # print(massLine)
        self.massList = massLine
        # return massLine

    def take_pressures(self, line):
        ppLine = line.replace('"', '')
        ppLine = ppLine.split()
        # print(ppLine)
        self.date = str(ppLine[0] + "\n"+ ppLine[1] +ppLine[2] + "\n" )
        self.datesList.append(self.date)


        ppLine = ppLine[4:-1]
        # print(len(ppLine))
        ppLine = [float(x) for x in ppLine]
        for i in range (0,len(ppLine)):
            if ppLine[i] < 1e-13:
                 ppLine[i]= 1e-13
                # print(i)

        # print(ppLine)
        self.pressuresList.append(ppLine)

        self.n_scans += 1
        # textForNScansLine = ("1/"+str(self.n_scans) )
        self.nScanLine.setText(self.date+ "1/" + str(self.n_scans - 1))
        self.slider.setMaximum(self.n_scans - 1)
        # print(self.n_scans)

    def doPlot(self, nscan):
        self.plot.clear()

        self.plot.plot(self.massList, self.pressuresList[nscan])

        self.nScanLine.setText(self.datesList[nscan]+ str(nscan + 1) + "/" + str(self.n_scans))

    def sliderChange(self, event):

        self.doPlot(event)

    def get_pessures(self):
        return self.pressuresList[self.slider.value()]

    def get_masses(self):
        return self.massList


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = PlotScan()
    mainWin.show()
    sys.exit(app.exec_())

import sys

from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QAction, QDialog, \
    QVBoxLayout, QMessageBox
from PyQt5.QtCore import Qt
from BackGroundSubtractor import MainWidget
# Subclass QMainWindow to customise your application's main window

class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.setWindowTitle("RGA Subtractor")
        self.mainSub=MainWidget()

        self.menubar = self.menuBar()
        self.help = self.menubar.addMenu("&Help")

        self.help_action = QAction("Show Help",self)

        self.help.addAction(self.help_action)

        self.help_action.triggered.connect(self.show_help)

        # helpMenu.addAction(self.aboutAction)



        self.setCentralWidget(self.mainSub)

    def show_help(self):
        a= "RGA Substractor. \n" \
           "By Lluís Ginés. \n \n" \
           "This is a tool for subtracting " \
           "MKS RGA Spectrums.\n" \
           "Files must be in .txt format and no edited at all\n" \
           "Choose you file by 'Select File' button\n" \
            "The file name will appears in the bottom box" \
           "Once both files are selected, the subtraction\n" \
           "will be shown in the bottom plot\n" \
           "If the file contains more than one scan\n" \
           "you can navigate through them via the slider\n" \
           " in the bottom.\n The number of the scan will be \n" \
           "shown as well" \
           " " \
           "If your files have been not acquired with the same number \n" \
           "of peaks, don't worry! the subtraction will be done \n" \
           "with the smaller resolution\n" \
           "For logarithmic representation, rigth click in the plot \n" \
           "-> plot options -> transforms -> log Y \n" \
            "Suggestions/corrections are welcome! \n\n" \
           "https://gitlab.com/LluisG/rgasubstractor"


        font = QtGui.QFont()
        font.setBold(True)

        msg = QMessageBox()
        msg.setWindowTitle("RGA Subtractor Help")
        msg.setText(a)
        msg.exec_()  # this will show our messagebox




    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:


        self.mainSub.endUpdateLoop.set()
        self.mainSub.hasFiles.set()

app = QApplication(sys.argv)

window = MainWindow()
window.show()
ret = app.exec_()
sys.exit(ret)
# app.exec_()

import sys
# from taurus.qt.qtgui.application import TaurusApplication
# from pyqtgraph.Qt import QtGui, QtCore
# import pyqtgraph as pg


# import numpy as np
import os
from PyQt5 import Qt, uic


# python -m pyqtgraph.examples


# TODO: check format files.

class Conversor(Qt.QWidget):

    def __init__(self, parent=None):
        # call the parent class init
        Qt.QWidget.__init__(self, parent=parent)
        # load the .ui file
        uic.loadUi(os.path.join(os.path.dirname(__file__), "converter.ui"), self)
        self.bChoose.clicked.connect(self.chooseFile)
        self.bConv.clicked.connect(self.callToConvert)

    def chooseFile(self):
        """to check"""
        fileName, _ = Qt.QFileDialog.getOpenFileName(self, 'txt rga files',
                                                     Qt.QDir.homePath(), '*.txt')
        if fileName:
            self.lineEdit.setText(fileName)

    def callToConvert(self):
        name = self.lineEdit.text()
        if name:
            self.convert(self.lineEdit.text())
        else:
            Qt.QMessageBox.warning(self, "No file selected", "Ensure to choose a file")

    def convert(self, file):
        with open(file) as f:

            for line in f:
                if line.startswith('"Instrument Name"'):
                    self.instrumentName = line.split('\t')[1].strip('"')
                    print(self.instrumentName)
                elif line.startswith('"Serial number"'):
                    self.serialNumber = line.split('\t')[1].strip('"')
                    print(self.serialNumber)
                elif line.startswith('"Time"'):
                    self.takeMassLine(line)
                for line in f:
                    self.takePressureAnDateLine(line)



    def takeMassLine(self, line):
        """take masses list in float format"""
        massLine = line.replace('"Mass ', "")
        massLine = massLine.replace('"', '')
        massLine = massLine.split()
        del massLine[0:2]
        del massLine[-3:]
        print(massLine)
        self.masList=massLine

    def takePressureAnDateLine(self,line):
        ppLine = line.replace('"', '')
        ppLine = ppLine.split()
        """date"""
        self.date = ppLine[0:3]
        print(self.date)
        if (self.date[2] == 'PM'):
            hora = self.date[1].split(":")
            hora[0] = int(hora[0]) + 12
            hora = (str(hora[0]) + ":" + hora[1] + ":" + hora[2])
            print("HORA", hora)
        self.date[0] = self.date[0].replace("/", "-")
        print(self.date)
        self.ppressures = ppLine[4:-1]
        print(self.ppressures)

if __name__ == "__main__":
    # Initialize a Qt application (Qt will crash if you do not do this first)
    app = Qt.QApplication(sys.argv)

    # instantiate the widget
    w = Conversor()
    w.setWindowTitle("RGA file Conversor")

    # show it (if you do not show the widget, it won't be visible)
    w.show()

    # Initialize the Qt event loop (and exit when we close the app)
    sys.exit(app.exec_())